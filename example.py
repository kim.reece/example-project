#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  3 13:45:25 2018

@author: Kim Reece <kim.reece@stud.uni-goettingen.de>
"""

import numpy as np
import holoviews as hv


def radial_metric(xk, x):
    """
    Radial distance with standard Euclidean metric between two points.

    Parameters
    ----------
    xk : array
        First point.
    x : array
        Second point.
    """
    assert len(xk) == len(x)
    return np.sqrt(np.sum([(xk[i]-x[i])**2 for i in range(0, len(x))]))


# TODO Math as written is a poor match for variable names and implementation
def F_gaussian(xk, x, c):
    """
    Gaussian radial distance function.

    .. math:: \\Phi(r) = \\exp(-r^2)

    Parameters
    ----------
    xk : array
        First point.
    x : array
        Second point.
    c : float
        Scaling factor.
    """
    return np.exp(-(radial_metric(xk, x)/c)**2)


def F_multiquad(xk, x, c):
    """
    Multiquadric radial distance function.

    .. math:: \\Phi(r) = \\sqrt{1 + r^2}

    Parameters
    ----------
    xk : array
        First point.
    x : array
        Second point.
    c : float
        Scaling factor.
    """
    return np.sqrt(1+(radial_metric(xk, x)/c)**2)


def F_invquad(xk, x, c):
    """
    Inverse quadratic radial distance function.

    .. math:: \\Phi(r) = (1 + r^2)^{-1}

    Parameters
    ----------
    xk : array
        First point.
    x : array
        Second point.
    c : float
        Scaling factor.
    """
    return (1+(radial_metric(xk, x)/c)**2)**-1


# TODO Use actual python classes for this
def new_instance():
    instance = {
            'n': 10,  # Number of values to use
            'd': 3,   # Dimension of input points
            'c': 1,   # Scaling factor
            'F': F_invquad,
            'nodes': [],
            'values': [],
            'coeffs': []
            }
    return instance


# Create a random function to approximate
# TODO move into testing
def randomize_instance(i):
    """
    Initializes a radial problem with randomized function data.

    Both nodes and values entries are filled up to the specified dimensions
    from a uniform distribution on [0,1).
    """
    i['nodes'] = np.random.rand(i['n'], i['d'])
    i['values'] = np.random.rand(i['n'], 1)


# TODO use a lambda function over F to handle parameters built into i
def f_term(i, x, j):
    """
    Individual term of the approximation.

    Only for use by f(i, x)!
    """
    return i['coeffs'][j]*i['F'](x, i['nodes'][j], i['c'])


def f(i, x):
    """
    Evaluate approximation from solved radial instance on a new point.
    """
    assert i['coeffs'] != []
    return np.sum([f_term(i, x, j) for j in range(0, i['n'])])


def solve_instance(i):
    """
    From a given radial instance calculate the coefficients of its solution.
    """
    A = [[i['F'](xi, xj) for xi in i['nodes']] for xj in i['nodes']]
    i['coeffs'] = np.linalg.solve(A, i['values'])


def error_instance(i):
    """
    Compare on the known nodes calculated vs. actual values.
    """
    soln = [f(i, i['nodes'][j]) - i['values'][j]
            for j in range(0, len(i['nodes']))]
    return {'min': np.min(soln),
            'max': np.max(soln),
            'avg': np.sum(soln)/len(soln)}


def vis1(i):
    m = 100
    # TODO 0xbadcode

    chosen = np.array([[i['nodes'][j][0],
                        i['values'][j]] for j in range(0, i['n'])])
    d0 = hv.Points(chosen).options({'Points': {'size': 5}})
    e0 = hv.Curve(([j/m for j in range(0, m)],
                   [f(i, [j/m, 0.5, 0.5]) for j in range(0, m)]))

    chosen = np.array([[i['nodes'][j][1],
                        i['values'][j]] for j in range(0, i['n'])])
    d1 = hv.Points(chosen).options({'Points': {'size': 5}})
    e1 = hv.Curve(([j/m for j in range(0, m)],
                   [f(i, [0.5, j/m, 0.5]) for j in range(0, m)]))

    chosen = np.array([[i['nodes'][j][2],
                        i['values'][j]] for j in range(0, i['n'])])
    d2 = hv.Points(chosen).options({'Points': {'size': 5}})
    e2 = hv.Curve(([j/m for j in range(0, m)],
                   [f(i, [0.5, 0.5, j/m]) for j in range(0, m)]))
    return (d0 * e0) + (d1 * e1) + (d2 * e2)


# gbbfbvvbvnddx kim
